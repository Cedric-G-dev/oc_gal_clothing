# Generated by Selenium IDE
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class TestAddarticle():
  def setup_method(self, method):
    self.driver = webdriver.Firefox()
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_addarticle(self):
    # Test name: Add_article
    # Step # | name | target | value
    # 1 | open | / | 
    self.driver.get("http://0.0.0.0:8000/")
    # 2 | setWindowSize | 1010x870 | 
    self.driver.set_window_size(1010, 870)
    # 3 | click | css=.site-menu > li:nth-child(2) > a | 
    self.driver.find_element(By.CSS_SELECTOR, ".site-menu > li:nth-child(2) > a").click()
    # 4 | click | css=.block-4-image .img-fluid | 
    self.driver.find_element(By.CSS_SELECTOR, ".block-4-image .img-fluid").click()
    # 5 | click | id=id_quantity | 
    self.driver.find_element(By.ID, "id_quantity").click()
    # 6 | type | id=id_quantity | 1
    self.driver.find_element(By.ID, "id_quantity").send_keys("1")
    # 7 | click | id=id_colour | 
    self.driver.find_element(By.ID, "id_colour").click()
    # 8 | select | id=id_colour | label=green
    dropdown = self.driver.find_element(By.ID, "id_colour")
    dropdown.find_element(By.XPATH, "//option[. = 'green']").click()
    # 9 | click | css=#id_colour > option:nth-child(2) | 
    self.driver.find_element(By.CSS_SELECTOR, "#id_colour > option:nth-child(2)").click()
    # 10 | click | id=id_size | 
    self.driver.find_element(By.ID, "id_size").click()
    # 11 | select | id=id_size | label=s
    dropdown = self.driver.find_element(By.ID, "id_size")
    dropdown.find_element(By.XPATH, "//option[. = 's']").click()
    # 12 | click | css=#id_size > option:nth-child(2) | 
    self.driver.find_element(By.CSS_SELECTOR, "#id_size > option:nth-child(2)").click()
    # 13 | click | css=.btn | 
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
  
